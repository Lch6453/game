'''
学号：20183411
作者：李丞灏
日期：2020.3.20
'''
tank = ["1.项羽","2.张飞","3.牛魔","4.白起","5.东皇太一","6.猪八戒","7.苏烈","8.廉颇","9.太乙真人"]
mage = ["1.张良","2.干将莫邪","3.诸葛亮","4.甄姬","5.墨子","6.王昭君","7.女娲","8.貂蝉","9.周瑜"]
archer = ["1.虞姬","2.孙尚香","3.黄忠","4.鲁班","5.伽罗","6.马可波罗"]
soldier = ["1.曜","2.亚瑟","3.关羽","4.刘备","5.吕布","6.马超"]
aided = ["1.姜子牙","2.孙膑","3.瑶","4.明世隐","5.鬼谷子"]
assassin = ["1.兰陵王","2.李白","3.孙悟空","4.阿珂","5.云中君","6.赵云"]
x = 3
while x!=0:
   print("有以下职业：\n1.坦克 2.法师 3.射手 4.战士 5.辅助 6.刺客\n")
   w = int(input("请输入您想要查看的职业,输入0退出：\n"))
   if (w==1):
       for item in (tank):
           print(item)
       print ("您想要删除或修改这里面的人物吗？")
       a = int(input("1.需要  2.不需要\n"))
       if (a==1):
           b = int(input("请输入您想要修改或删除的人物:\n"))
           print(tank[b-1])
           c = int(input("请输入您想要进行的操作：1.修改  2.删除\n"))
           if (c==1):
              d = str(input("请输入修改后的结果：\n"))
              tank[b-1] = d
           elif (c==2):
              del tank[b-1]
   elif (w==2):
       for item in (mage):
           print(item)
       print("您想要删除或修改这里面的人物吗？")
       a = int(input("1.需要  2.不需要\n"))
       if (a == 1):
           b = int(input("请输入您想要修改或删除的人物:\n"))
           print(mage[b - 1])
           c = int(input("请输入您想要进行的操作：1.修改  2.删除\n"))
           if (c == 1):
               d = str(input("请输入修改后的结果：\n"))
               mage[b - 1] = d
           elif (c == 2):
                 del mage[b - 1]
   elif (w==3):
       for item in (archer):
           print(item)
       print("您想要删除或修改这里面的人物吗？")
       a = int(input("1.需要  2.不需要\n"))
       if (a == 1):
           b = int(input("请输入您想要修改或删除的人物:\n"))
           print(archer[b - 1])
           c = int(input("请输入您想要进行的操作：1.修改  2.删除\n"))
           if (c == 1):
              d = str(input("请输入修改后的结果：\n"))
              archer[b - 1] = d
           elif (c == 2):
               del archer[b - 1]
   elif (w==4):
       for item in (soldier):
           print(item)
       print("您想要删除或修改这里面的人物吗？")
       a = int(input("1.需要  2.不需要\n"))
       if (a == 1):
           b = int(input("请输入您想要修改或删除的人物:\n"))
           print(soldier[b - 1])
           c = int(input("请输入您想要进行的操作：1.修改  2.删除\n"))
           if (c == 1):
               d = str(input("请输入修改后的结果：\n"))
               soldier[b - 1] = d
           elif (c == 2):
               del soldier[b - 1]
   elif (w==5):
       for item in (aided):
           print(item)
       print("您想要删除或修改这里面的人物吗？\n")
       a = int(input("1.需要  2.不需要"))
       if (a == 1):
           b = int(input("请输入您想要修改或删除的人物:\n"))
           print(aided[b - 1])
           c = int(input("请输入您想要进行的操作：1.修改  2.删除\n"))
           if (c == 1):
               d = str(input("请输入修改后的结果：\n"))
               aided[b - 1] = d
           elif (c == 2):
               del aided[b - 1]
   elif (w==6):
       for item in (tank):
           print(assassin)
       print("您想要删除或修改这里面的人物吗？")
       a = int(input("1.需要  2.不需要\n"))
       if (a == 1):
           b = int(input("请输入您想要修改或删除的人物:\n"))
           print(assassin[b - 1])
           c = int(input("请输入您想要进行的操作：1.修改  2.删除\n"))
           if (c == 1):
               d = str(input("请输入修改后的结果：\n"))
               assassin[b - 1] = d
           elif (c == 2):
                 del assassin[b - 1]
   elif (w==0):
       break
   else:
       print("无其它选项，请检查并再次输入\n")


